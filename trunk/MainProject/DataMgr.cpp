#include "DataMgr.h"

void DataMgr::init( std::string fileName )
{
    // a file with data that may be altered
    dataFileName = fileName;
    // initialise everything to default values
    createDefaultConfigData( );
    // checking if the files exist
    if ( ! Utils::isFileExist( dataFileName.c_str() )  )
        // found no files or could not access them, using the default settings
        std::cout << "DataMgr.init(). Using DEFAULT Settings\n" << std::endl;
    else
        readSystemData();
    // get the fixed data from separate file
    std::string fixedFileName = "data/DevOptions.xml";
    if ( ! Utils::isFileExist( fixedFileName.c_str() )  )
        std::cout << "DataMgr.init(). can not find " << fixedFileName << std::endl;
    else
        readDataLists( fixedFileName );
}

std::vector<std::string> DataMgr::getDataList( std::string name )
{
    return dataLists[ name ];
}

std::string DataMgr::getSystemSetting( std::string name ) const
{
    if ( systemSettings.find( name ) != systemSettings.end() )
        return systemSettings.find( name )->second;
    else if ( soundSettings.find( name ) != soundSettings.end() )
        return soundSettings.find( name )->second;
    else if ( gameSettings.find( name ) != gameSettings.end() )
        return gameSettings.find( name )->second;
    else
    {
        std::cout << "getSystemSetting() failed: Could not find " << name << std::endl;
        return "";
    }
}

void DataMgr::setSystemSetting( std::string name, std::string value )
{
    if ( systemSettings.find( name ) != systemSettings.end() )
        systemSettings[ name ] = value;
    else if ( soundSettings.find( name ) != soundSettings.end() )
        soundSettings[ name ] = value;
    else if ( gameSettings.find( name ) != gameSettings.end() )
        gameSettings[ name ] = value;
    else
        std::cout << "setSystemSetting() failed: Could not find " << name << std::endl;
}

void DataMgr::createDefaultConfigData( )
{
    // initialise default irrlicht device values
    systemSettings["bpp"] = "32"; //"16";
    systemSettings["device"] = "OpenGL"; //"Software"; "DirectX9";
    systemSettings["width"] = "800";
    systemSettings["height"] = "600";
    systemSettings["fullscreen"] = "false";
    systemSettings["antialias"] = "true";
    systemSettings["stereobuffer"] = "true";
    systemSettings["highprecisionfpu"] = "true";
    systemSettings["stencilbuffer"] = "false";
    systemSettings["vsync"] = "false";   // set to true if fullscreen and image tearing
    systemSettings["withalphachannel"] = "true";
    systemSettings["zbufferbits"] = "true";
    systemSettings["doublebuffer"] = "true";
    // sound settings
    soundSettings["soundon"] = "true";
    soundSettings["soundvolume"] = "1";    // 0 to 1
    soundSettings["music"] = "true";    // 0 to 1
    // application title and last level played
    gameSettings["version"] = "Irr Game Level v1.0";
    gameSettings["name"] = "bob dole";
    gameSettings["startlevel"] = "1";
}

bool DataMgr::readDataLists( std::string fileName )
{
    try
    {
        ticpp::Document doc( fileName );
        doc.LoadFile();                                       // exception thrown
        ticpp::Element* rootNode = doc.FirstChildElement( "useroptions", true );
        //! read the data for the configuration lists into a vector
        std::vector<std::string> options;
        // read the device renderer list selections
        ticpp::Element* baseNode = rootNode->FirstChildElement( "deviceoptions" );
        ticpp::Element* node = baseNode->FirstChildElement( "devices" );
        ticpp::Iterator< ticpp::Element > iter;
        for ( iter = iter.begin( node );  iter != iter.end(); ++iter )
        {
            options.push_back( ( *iter ).GetText() );
        }
        // save a COPY of the vector in the std::map under the name of devices
        dataLists[ "devices" ] = options;
        // clear the vector list
        options.clear();
        // get the screen resolutions list
        node = baseNode->FirstChildElement( "resolutions" );
        for ( iter = iter.begin( node );  iter != iter.end(); ++iter )
        {
            options.push_back( ( *iter ).GetText() );
        }
        dataLists[ "resolutions" ] = options;
        options.clear();
        // get the bpp options
        node = baseNode->FirstChildElement( "bpps" );
        for ( iter = iter.begin( node );  iter != iter.end(); ++iter )
        {
            options.push_back( ( *iter ).GetText() );
        }
        dataLists[ "bpps" ] = options;
    }
    catch ( ticpp::Exception& error )
    {
        std::cout << "readDataLists() TiCPP Error: " << error.what() << std::endl;
        return false;
    }
    return true;
}

bool DataMgr::readSystemData( )
{
    try
    {
        ticpp::Document doc( dataFileName );
        doc.LoadFile();                                       // exception thrown
        ticpp::Element* rootNode;
        rootNode = doc.FirstChildElement( "systemsettings", true );
        //! get the irrlicht system settings data
        // find the node to start from
        ticpp::Element* baseNode = rootNode->FirstChildElement( "deviceconfig" );
        // iterate through the xml nodes
        ticpp::Iterator< ticpp::Element > iter;
        for ( iter = iter.begin( baseNode ); iter != iter.end(); ++iter )
        {
            // get the map index name and assign the found value to it
            systemSettings[ XmlUtils::getElementName( *iter ) ] = ( *iter ).GetText();
        }
        //! read the game start up settings
        baseNode = rootNode->FirstChildElement( "gameconfig" );
        for ( iter = iter.begin( baseNode ); iter != iter.end(); ++iter )
        {
            // get the map index name and assign the found value to it
            gameSettings[ XmlUtils::getElementName( *iter ) ] = ( *iter ).GetText();
        }
        //! read the game sound settings
        baseNode = rootNode->FirstChildElement( "soundconfig" );
        for ( iter = iter.begin( baseNode ); iter != iter.end(); ++iter )
        {
            // get the map index name and assign the found value to it
            soundSettings[ XmlUtils::getElementName( *iter ) ] = ( *iter ).GetText();
        }
        //! debug output of the loaded settings
        //! print the map contents out to the console
        std::cout << "Begin System Settings List" << std::endl;
        std::map<std::string, std::string>::iterator mapIter;
        for ( mapIter = systemSettings.begin(); mapIter != systemSettings.end(); mapIter++ )
        {
            // the index name (.first) and the value (.second) it contains
            std::cout << ( *mapIter ).first << "\t" << ( *mapIter ).second << std::endl;
        }
        std::cout << std::endl << "End System Settings List" << std::endl;
    }
    catch ( ticpp::Exception& error )
    {
        std::cout << "readSystemData() TiCPP Error: " << error.what() << std::endl;
        return false;
    }
    return true;
}

bool DataMgr::saveSystemSettings(  )
{
    try
    {
        // Create an empty document, that has no name
        ticpp::Document doc;
        // create minimal XML file declaration
        ticpp::Declaration* decl = new ticpp::Declaration( "1.0", "UTF-8", "" );
        // add the declaration to the doc
        doc.LinkEndChild( decl );
        // create the usual decriptive comment
        ticpp::Comment* comment = new ticpp::Comment();
        comment->SetValue( " the basic game configuration data " );
        // add the comment to the doc
        doc.LinkEndChild( comment );
        //! create the doc root node - has no attributes
        ticpp::Element* rootNode = new ticpp::Element( "systemsettings" );
        doc.LinkEndChild( rootNode );
        ticpp::Comment* comment1 = new ticpp::Comment();
        comment1->SetValue( " the irrlicht parameters for createDeviceEx() " );
        rootNode->LinkEndChild( comment1 );
        //
        //! create the node to start with
        ticpp::Element* baseNode = new ticpp::Element( "deviceconfig" );
        // for each setting use the map index name as the element name and add its value
        std::map<std::string, std::string>::iterator mapIter = systemSettings.begin();
        while ( mapIter != systemSettings.end() )
        {
            // make the element name same as the map's index name
            ticpp::Element* element = new ticpp::Element( ( *mapIter ).first );
            // set the element's value from the map
            element->SetText( ( *mapIter ).second );
            // link it to the base node
            baseNode->LinkEndChild( element );
            ++mapIter;
        }
        rootNode->LinkEndChild( baseNode );
        //
        //! save the game settings
        ticpp::Comment* comment2 = new ticpp::Comment();
        comment2->SetValue( " game configuration settings " );
        rootNode->LinkEndChild( comment2 );
        baseNode = new ticpp::Element( "gameconfig" );
        // for each setting use the map index name as the element name and add its value
        mapIter = gameSettings.begin();
        while ( mapIter != gameSettings.end() )
        {
            ticpp::Element* element = new ticpp::Element( ( *mapIter ).first );
            element->SetText( ( *mapIter ).second );
            baseNode->LinkEndChild( element );
            ++mapIter;
        }
        rootNode->LinkEndChild( baseNode );
        //
        //! save the sound settings
        ticpp::Comment* comment3 = new ticpp::Comment();
        comment3->SetValue( " game sound settings " );
        rootNode->LinkEndChild( comment3 );
        baseNode = new ticpp::Element( "soundconfig" );
        mapIter = soundSettings.begin();
        while ( mapIter != soundSettings.end() )
        {
            ticpp::Element* element = new ticpp::Element( ( *mapIter ).first );
            element->SetText( ( *mapIter ).second );
            baseNode->LinkEndChild( element );
            ++mapIter;
        }
        rootNode->LinkEndChild( baseNode );
        // write to the given file name
        doc.SaveFile( dataFileName );
        std::cout << "saveSystemSettings() successful" << std::endl;
    }
    catch ( ticpp::Exception& ex )
    {
        std::cout << "saveSystemSettings() Save XML File Error: " << ex.what() << std::endl;
        return false;
    }
    return true;
}
