#include "GameMgr.h"
// include the starting level
#include "StartMenu.h"

int main()
{
    GameMgr game;
    // initialisation will (eventually) init sound, physics, AI, level data, etc
    // a DataMgr will be used to load start up data in xml format
    if ( ! game.initGameMgr( ) )
        return EXIT_FAILURE;

    // TODO change to create instance of your start/intro class name
    game.changeState( StartMenu::getInstance() );

    // start the game loop
    game.run();

    // clean up the game systems
    game.freeResources();

    cout << "Farewell world! " << endl;
    return EXIT_SUCCESS;
}

