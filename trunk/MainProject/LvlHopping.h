#ifndef LVL_HOPPING
#define LVL_HOPPING

#include <iostream>
#include <vector>


using std::cout;
using std::endl;


#include "irrlicht.h"

using namespace irr;
// everything in the Irrlicht Engine is in the 'irr' namespace
// the 5 sub namespaces of the 'irr' namespace
//using namespace core;
//using namespace scene;
//using namespace video;
//using namespace io;
//using namespace gui;
// load the irrklang sound library


// triangle selectors for collisions
// #include "Irr_Utils.h"
// string handling functions and string <-> number conversions
// #include "Utils.h"
// stub class for game engine
#include "GameMgr.h"
#include "PauseGUI.h"
#include "OptionsGUI.h"
#include "IrrUtils.h"
#include "utils.h"
#include "RakNetIncludes.h"
#include "DataMgr.h"
#include "Entity.h"
#include "irrArray.h"


// include the cloudscenenode header
#include "CloudSceneNode.h"



/** this class uses the Singleton pattern to ensure there is only ever one instance.
  This class will (eventually) be created by a State Manager class.
  This class is for a single level within a game.
  There can be multiple levels but only one active at any time */
class LvlHopping : public IState
{
public:
    /* (1 and 3 are constants - Set them according to how many elements of the respective entity you intend to load into the game level )
     * 1. The amount of static meshes that are going to be loaded into this level.
     * 2. Declare static level meshes as a MeshSceneNode array, which will contain all static level meshes.
     * 3. The amount of clouds that are going to be loaded into this level.
     * 4. Declare clouds as a CloudSceneNode array, which will contain all moving clouds. */
    const static int MAX_LEVEL_OBJECTS = 17;
    const static int MAX_CLOUD_LAYERS = 3;
    scene::IMeshSceneNode* objMesh[ MAX_LEVEL_OBJECTS ];
    scene::CCloudSceneNode* cloudLayer[ MAX_CLOUD_LAYERS ];

private:
    //! particale effects
    void createParticleImpacts();
    //! wide character for player name
    WCHAR playerName;

    //! billboard which displays the player name in-game
    core::array<scene::IBillboardTextSceneNode*> billboardNameArray;
    scene::IBillboardTextSceneNode* billName;

    //! RakNet Variables
    RakNet::RakPeerInterface *peer;
    char serverPort[30], clientPort[30];
    //bool b;
    char str[256];
    // Holds packets
    RakNet::Packet* packet;
    //! checks if you are the server
    bool isServer;
    //! connect/join to a server
    bool rakConnect();
    //! sends/receives and updates server data (movement, shooting)
    void rakUpdate();
    //! number of people in the server
    int conNum;
    //! checks if you are dead
    bool iAmDied;
    //! checks if you are shooting
    bool shooting;
    //! how much ammo
    int tempClipAmmo;
    //! have you disconnected
    bool disconnect;
    //! A boolean to hold whether the client needs to send a score update to the server.
    bool clientScoreUpdate;
    //! shooting trajectory
    core::vector3df sStart;
    core::vector3df sEnd;
    //! delay time between jump
    bool jumpDelay;
    //! if sound is on or not
    bool soundEnabled;
    //}

//! enumerator for teams
    enum Team
    {
        TEAM_NONE = 0,
        TEAM_REBEL = 1,
        TEAM_IMPERIAL = 2,
    };

    //! background image
    video::ITexture*bgImage;

    //{ Sounds Section

    // sound reference for anu sounds that will use 3D fx
    //! game music
    ISoundSource* music;
    ISoundSource* lvlOneBgMusic;
//  ISound* lvlTwoBgMusic;
//  ISound* titleBgMusic;
    //! bullet impact sounds
    ISoundSource* impactSound;
    ISoundSource* impactTwoSound;
    ISoundSource* impactThreeSound;
    ISoundSource* impactFourSound;
    ISoundSource* impactFiveSound;
    //! rifle shooting
    ISoundSource* shotSound;
//  ISoundSource* cannonShotSound;
    //! player death sound
    ISoundSource* deathSound;
    //! pistol shooting
    ISoundSource* shotTwoSound;
    //! minigun shooting
    ISoundSource* shotThreeSound;
    //! blunderbuss shooting
    ISoundSource* shotFourSound;
    //! jumping sound
    ISoundSource* jumpSound;
    //! picking up health sound
    ISoundSource* healthSound;
    //! rifle pickup
    ISoundSource* pickupSoundOne;
    //! pistol pickup
    ISoundSource* pickupSoundTwo;
    //! minigun pickup
    ISoundSource* pickupSoundThree;
    //! blunderbuss pickup
    ISoundSource* pickupSoundFour;
//  ISoundSource* flagTakenSound;
//  ISoundSource* flagResetSound;
//  ISoundSource* buttonPressSound;
//  ISoundSource* victorySound;
//  ISoundSource* defeatSound;
//! reloading sound
    ISoundSource* reloadSound;
    //! collision sound
    ISoundSource* hitSound;
    //! yell reload sound
    ISoundSource* reloadCallSound;
    //! clicks when empty or attempts to shoot while empty
    ISoundSource* gunEmptySound;


    //} Sounds Section

    // class ref. to the gui objects for interaction
    //! resume game
    gui::IGUIButton* cmdResume;
    //! option menu
    gui::IGUIButton* cmdOptions;
    //! go to main menu
    gui::IGUIButton* cmdExitGame;
    //! quit game to desktop
    gui::IGUIButton* cmdExitWindows;


    //{ Entities (Ammo Pickup, Health, Powerups etc)
    //! array for entities (guns, health)
    Entity entityArray[512];
    //! health entity
    Entity healthDrop;
    //! rifle entity
    Entity rifleAmmoDrop;
    //! pistol entity
    Entity pistolAmmoDrop;
    //! minigun entity
    Entity gattlingAmmoDrop;
    //! blunderbuss entity
    Entity blunderbussAmmoDrop;
    //! flag entity
    Entity objectiveFlagDrop;
    // pointer which points to each ammo box type in turn in order to test against it.
    Entity* entityBox;
    //! health model
    scene::IMeshSceneNode* healthMesh;
    scene::IMeshSceneNode* healthMesh2;
    scene::IMeshSceneNode* healthMesh3;
    //! rifle model
    scene::IMeshSceneNode* rifleAmmoMesh;
    scene::IMeshSceneNode* rifleAmmoMesh2;
    //! pistol model
    scene::IMeshSceneNode* pistolAmmoMesh;
    //! minigun model
    scene::IMeshSceneNode* gattlingAmmoMesh;
    //! blunderbuss model
    scene::IMeshSceneNode* blunderbussAmmoMesh;
    //! imperial team flag model
    scene::IMeshSceneNode* imperialFlagMesh;
    //! rebel team flag model
    scene::IMeshSceneNode* rebelFlagMesh;
    //! test picking up entities
    bool testEntityPickup( Entity* entityBox );
    // used in the testEntityPickup function for the distance between two points
    core::vector3df tempVectorEntity;
    core::vector3df tempVectorEntity2;
    float distance;
    //}

    // gui image
    video::ITexture* iguiwindowimage;


    // active GUI elements need an ID
    //! enumerator for button commands
    enum
    {
        GUI_CMD_RESUME,
        GUI_CMD_OPTIONS,
        GUI_CMD_EXITGAME,
        GUI_CMD_EXITWINDOWS,
        GUI_CHK_SOUND_ON,                // mute sound on or off
        GUI_SCR_VOLUME,                 // scroll bar for volume setting
        GUI_BTN_BACK,
        GUI_BTN_EXIT_GAME = 1001,            // play game
        GUI_BTN_EXIT_WINDOW,                 // quit
        GUI_BTN_OPTIONS,                // list of devices etc
        GUI_BTN_RESUME,                // list of creators
    };
    //! enumerator for movement controls
    // name for the components

    gui::IGUIButton* btnBack;
    gui::IGUIButton* btnExitGame;
    gui::IGUIButton* btnExitWindow;
    gui::IGUIButton* btnOptions;
    gui::IGUIButton* btnResume;

    //GUI elements for the Pause options

    gui::IGUIScrollBar* scrVolume;
    gui::IGUICheckBox* chkSoundOn;

    enum
    {
        MOVE_FORWARD,
        MOVE_BACKWARD,
        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_JUMP,
        ACTION_RELOAD,
        ACTION_IDLE,
    };


    //! derelicht level mesh/model
    scene::IMeshSceneNode* lvlOne;

    //scene::IAnimatedMeshSceneNode* target;
    video::ITexture* mouseCursor;
    video::ITexture* mousePause;
    //! player object
    Player* pPlayerObject;
//	scene::ITriangleSelector* buildingSelector;
    unsigned targetCount;
    core::array<scene::ITriangleSelector*> spheresArray;
    core::array<scene::IParticleSystemSceneNode*> flamesArray;

    //! player's rotation
    core::vector3df tempRotation;
    //! player's position
    core::vector3df tempPosition;
    //! The Mesh of the Player
    scene::IAnimatedMeshSceneNode* friendMesh;
    //! player's current animation
    void selectAnimation();
    int currentAction;
    int newAnimation;

    int newWeapon;
    int sendAnimation;

    //! Ship meshes
    scene::IMeshSceneNode* ship1Mesh;
    scene::IMeshSceneNode* ship2Mesh;
    scene::IMeshSceneNode* ship1AdditionsMesh;
    scene::IMeshSceneNode* ship2AdditionsMesh;
    scene::IMeshSceneNode* FlabioMesh;

    //! A placeholder for height maps, used in initWorld() when loading textures/materials.
    video::ITexture* bumpMap;
    //! A placeholder for file names, used in initWorld() when loading meshes and texture files.
    io::path fileTemp;
    //! size of the camera in-game
    core::vector3df cameraSize;
    core::stringw tempstring;
    //! bullet damage
    int tempDamageInt;

    //! 3D clouds
    //- keeping reference as used in different functions
    scene::ITerrainSceneNode* terrainMesh;
//    scene::CCloudSceneNode* cloudLayer1;
//    scene::CCloudSceneNode* cloudLayer2;
//    scene::CCloudSceneNode* cloudLayer3;
    scene::CCloudSceneNode* cloudLayer4;


    //{ Bullets&Weapons section

    //! Struct which contains bullet-related info
    struct Bullets
    {
        //! variable which holds the bullet node.
        scene::ISceneNode* bulletNode;

        //! variable which holds the timeout in ms of the bullet (to test in update)
        u32 timeout;

        //! the time at which the bullet was fired
        unsigned int startTime;
    };

    //! animation used in everything
    scene::ISceneNodeAnimator* anim;
    //! test Bullet Player Collision
    scene::ISceneNode* collidedNode;

    //! subroutine which handles the firing of the bullets.
    void shoot( int selected, int incoming = 0, core::vector3df position = core::vector3df( 0, 0, 0 ), core::vector3df endPoint = core::vector3df( 0, 0, 0 ) );

    //! subroutine which flashes the weapon outlines at the bottom of the screen depending on which ones are available to use.
    void flashWeapons();

    //! variable which holds the delay for bullet collision testing
    int timeDelay;

    //! variable to hold the status (dead or alive) of the other player.
    bool enemyHit;

    //! A variable which holds whether the primary weapon was the last to fire (Only used in case of dual wield)
    bool primaryWeaponFired;

    //! variable which holds the Triangle Selection of all Bullet-collisible objects (with ID of '1')
    scene::IMetaTriangleSelector* worldCollisionSelector;

    //! variable which holds the Triangle Selection of all Bullet-collisible objects (with ID of '1')
    scene::IMetaTriangleSelector* enemyCharacterSelector;

    //! A boolean which holds the held state of the mouse button, dynamically altered in mouseEvent.
    bool isFiring;

    //! Variable which holds the mesh of the rifle
    irr::scene::IAnimatedMeshSceneNode* gun;

    //! Variable which holds the mesh of the first pistol
    irr::scene::IAnimatedMeshSceneNode* gun2;

    //! Variable which holds an int representation of which weapon is currently selected
    int weaponSelection;

    //! function which initialises the player's weapon (called each time the player newly aquires one)
    bool initGuns( int selected );


    //! Stores the time that has passed since the last shot was fired
    unsigned int shotTime;

    //!Struct used to contain the information relating to the bullet impact
    struct SParticleImpact
    {
        u32 when;
        core::vector3df pos;
        core::vector3df outVector;
    };
    core::array<SParticleImpact> Impacts;

    //! function which tests to see whether the bullet has collided with the player.
    SParticleImpact testBulletPlayerCollision( core::line3d<f32> line );

    /** \brief randomly generate small (negative or positive)number proportionate to the distance between start and end, then apply it to a randomly chosen number of randomly chosen attribute(s) of the end point(x,y or z) to imitate bullet spread.
     * \param start the Starting point of the bullet trajectory
     * \param end the precise Ending point of the bullet trajectory
     * \return the new Ending point slightly randomised to almost-but-not-quite hit it's target with 100% accuracy.
     */
    core::vector3df spreadBullet( core::vector3df end );

    //} Bullets&Weapons section

public:
    //! shows your GUID
    RakNet::RakNetGUID guidmy;
    //! shows how much ammo you got
    int ammoCount;
    //! shows how many people are in the server
    int serverCount;
    //! how many people in each team
    int imperialPlayers;
    int rebelPlayers;
    //! makes sure that the dead player doesn't lose health when they respawn
    bool clientDiedOnce;
    //! team kills
    int imperialKills;
    int rebelKills;
    //! updates the client side
    bool clientsNeedUpdate;
    //! has game started?
    bool started;

    //!see if you are connected to a server
    bool isConnected;
    //!boolean for pause
    bool isPaused;
    //! cooldown for bullet shooting
    float cooldown;
    //! timer for back to idle anim
    float idleTime;
    //! cooldown for death - "For whom the bell tolls"
    float theBell;
    //! warm up for gattling gun
    float warmUp;
    //! Initialise the game objects. Calls the private init...() functions.
    /** The State Manager class will take over most of the device initialisation
    as there should only be a single reference to each of the Irrlicht devices
    \param the title for the window
    \return false if there any initialisation errors */
    bool initState( GameMgr& game );

    //! free any resources on the heap
    /** delete any objects created by new and drop any Irrlicht objects
    made by functions beginning with create, such as createDevice() */
    void freeResources( );

    //! to detect keyboard presses.
    /** Each key can be toggled on or off
    \param key char to test if down or not
    \return from the array - true if key is down */
    bool getKey( unsigned char key );

    //! Empty the key event array
    /** call at least once at the level init, */
    void clearKeys();

    //! The main game loop update, call any other update functions from here.
    /** Anything can be drawn between a beginScene() and endScene() call.
    The beginScene clears the screen with a colour and depth buffer if wanted.
    The Scene Manager and the GUI Environment then draw their content.
    The endScene() call presents everything on the screen. */
    void update();

    //! timer pause
    void pause();
    //! timer resume
    void resume();

    //! singleton class creation
    static LvlHopping* getInstance()
    {
        return &instance;
    }

private:
    //! static instance reference
    static LvlHopping instance;
    //! inline private ctor
    /** prevent "accidental" attempts to create an instance of the class */
    LvlHopping( ) {}

    //! inline private non virtual dtor
    /** Inheritance is not allowed.
    This class kills itself in freeResources() */
    ~LvlHopping()
    {
        cout << "LvlHopping deleted" << endl;
    }

    //! inline copy ctor
    /** prevent "accidental" copies */
    LvlHopping( const LvlHopping& );

    //! inline private assign operator
    /** prevent "accidental" assignments */
    LvlHopping& operator=( const LvlHopping& );

    //! init any general data, timers player/enemy/weapon states
    /** \return false if there any initialisation errors */
    bool initData();

    //! Init animated meshes and textures etc
    /** \return false if there any initialisation errors */
    bool initNodes();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUI();

    //! init game HUD
    bool initHUD();

    //! reload timers so that you can't shoot whilst reloading (animation time dependant)
    void reloadTimer();
    //! init the camera/s required for this level
    /** \return false if there any initialisation errors */
    bool initCameras();

    //! init the entities (pickups)
    /** return false if there are any initialisation errors */
    bool initEntities();

    //! restarts game so everyone has a fair game
    void startGame();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initWorld();

    bool killPlayer();
    //! re-init everything required to 'respawn' the player
    bool respawnPlayer();


    //! init the collisions for terrain, static or animated meshes etc
    /** \note call when all nodes and cameras are initialised
    \return false if there any initialisation errors */
    bool initCollisionDetection();

    //! init the sounds for this level
    /** \note call when all associated nodes are initialised
    \return false if there any initialisation errors */
    bool initSounds();

    //! key array to track which keys are down at any time
    /** \param KEY_KEY_CODES_COUNT: Irrlicht provided OS dependant constant
    for the number of keys available. */
    bool keys[ irr::KEY_KEY_CODES_COUNT ];

    //! key events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void keyboardEvent( SEvent event );

    //! mouse events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void mouseEvent( SEvent event );

    //! a mouse event happened on a GUI object
    /** \param the event passed from Irrlicht's event handler */
    void mouseGUIEvent( SEvent event );

    //! show FPS, poly count, active camera position and time
    void debugData();

    //! local level state, set to true in init()
    bool running;

    //! need at least one camera except for 2D only scenes
    scene::ICameraSceneNode* pCamera;
    scene::ICameraSceneNode* secondCamera;
    scene::ICameraSceneNode* PauseCamera;

    //! the debug display
    void displayDebugData();
    //! if true show the debug display
    bool showDebugData;
    //! the debug display on a label
    gui::IGUIStaticText* lblDebugData;
    gui::IGUIStaticText* ammo;
    gui::IGUIStaticText* imperialCount;
    gui::IGUIStaticText* rebelCount;
    gui::IGUIStaticText* health;
    gui::IGUIStaticText* lblIP;

    //! images for the Hud
    gui::IGUIImage* healthCog;
    gui::IGUIImage* ammoCog;
    gui::IGUIImage* insigniaOne;
    gui::IGUIImage* insigniaTwo;
    gui::IGUIImage* pistolOutline;
    gui::IGUIImage* rifleOutline;
    gui::IGUIImage* gattlingOutline;
    gui::IGUIImage* blunderbussOutline;
    video::ITexture* healthCogImage; //= "media/images/HealthTopRight.png";
    video::ITexture* ammoCogImage; //= "media/images/AmmoBottomLeft.png";
    video::ITexture* insigniaOneImage; //= "media/images/Imperialnavysmall.png";
    video::ITexture* insigniaTwoImage; //= "media/images/Jorganspiratessmall.png";
    video::ITexture* pistolOutlineImage; //= "media/images/PistolBottomMid.png";
    video::ITexture* rifleOutlineImage; //= "media/images/RifleBottomMid.png";
    video::ITexture* gattlingOutlineImage; //= "media/images/GettlingBottomMid.png";
    video::ITexture* blunderbussOutlineImage; //= "media/images/BlunderBottomMid.png";

    //! update local time keeper
    void updateTimer();
    //! Stores the complete time that has passed for this level
    unsigned int gameTime;
    //! the actual time taken to render a scene
    unsigned int timeDelta;
    //! time to reload the gun
    unsigned int reloadTime;
    //! ammo respawn timer
    unsigned int ammoRespawn;
    //! restarts the game to make it even
    unsigned int startRespawn;
    //! gun highlight timer
    unsigned int gunTimer;
    //! timer to hold when the animation is meant to end.
    unsigned int animationTimer;
    //! msecs since last frame - will need to adjust for pause / resume
    unsigned int timeLast;
    //! time now - will need to adjust for pause / resume
    unsigned int timeNow;
    //! desired render cycles per second
    unsigned int desiredFPS;
    //! Bool to hold whether the game has started for real yet.
    bool gameStarted;
    //! Bool to hold whether the button to start the game has been pressed.
    bool gameButton;
};

#endif // IRR_TEMPLATE_H


























