#ifndef GAME_SELECTION_HOPPING
#define GAME_SELECTION_HOPPING

#include <string>
#include <iostream>
using std::cout;
using std::endl;

#include "irrlicht.h"
#include "irrklang.h"
// everything in the Irrlicht Engine is in the 'irr' namespace
using namespace irr;
using namespace irrklang;

// string handling functions and string <-> number conversions
#include "Utils.h"
// game manager
#include "GameMgr.h"
#include "DataMgr.h"
#include "RakNetIncludes.h"

// include any classes that will be started from tis menu
#include "GameSettings.h"
#include "GameHelp.h"
#include "LvlDerelict.h"
#include "LevelSelection.h"


/** this class uses images for "buttons"
Shows navigation to another state/level from the current one.
No state stack is used and only one state is active at any time.
Each state will need to know which state comes next or previously if it needs to return. */
class GameSelectionHopping : public IState
{
public:
    char rakNetStateCS;
    RakNet::RakPeerInterface* peer;
    RakNet::RakNetGUID guidmy;


private:
    enum GUI
    {
        GUI_BTN_CREATE = 1001,            // Create your own game
        GUI_BTN_JOIN,                     // Join an existing game
        GUI_EDIT_SERVER,
        GUI_BTN_OPTIONS,                // list of devices etc
        GUI_BTN_HELP,                // list of creators
        GUI_BTN_CANCEL,               // quit
    };

    gui::IGUIButton* btnCreate;
    gui::IGUIButton* btnJoin;
    gui::IGUIButton* btnExit;
    gui::IGUIEditBox* edtServer;



    ISound *music;

    //! mouse cursor image
    video::ITexture* mouseCursor;
    //! mouse position - set in update
    core::position2d<s32> mousePos;
    /** @brief loadMouseCursor
      * the image to use for the mouse cursor
      */
    void loadMouseCursor( );
    /** @brief displayMouse
      * show the mouse image at the x y user moves to.
      */
    void displayMouse( );

    video::ITexture* imgBg;          // bg image

    gui::IGUIInOutFader* fader;         // start/finish fader after resource loading
    scene::ICameraSceneNode* pCamera;
    gui::IGUIStaticText* title;

    //! display your IP address to share to other user so they can join
    gui::IGUIStaticText* ipShare;

public:
    //rakpeer shuizz
//    RakNet::RakPeerInterface* peer;
//    RakNet::Packet* packet;

    //! Initialise the game objects. Calls the private init...() functions.
    /** The State Manager class will take over most of the device initialisation
    as there should only be a single reference to each of the Irrlicht devices
    \param the title for the window
    \return false if there are any initialisation errors */
    bool initState( GameMgr& game );

    //! free any resources on the heap
    /** delete any objects created by new and drop any Irrlicht objects
    made by functions beginning with create, such as createDevice() */
    void freeResources( );

    //! The main game loop update, call any other update functions from here.
    /** Anything can be drawn between a beginScene() and endScene() call.
    The beginScene clears the screen with a colour and depth buffer if wanted.
    The Scene Manager and the GUI Environment then draw their content.
    The endScene() call presents everything on the screen. */
    void update();

    //! timer pause
    void pause();
    //! timer resume
    void resume();

    //! singleton class creation
    static GameSelectionHopping* getInstance()
    {
        return &instance;
    }

private:
    //! static instance reference
    static GameSelectionHopping instance;

    //! inline private ctor
    /** prevent "accidental" attempts to create an instance of the class */
    GameSelectionHopping( )
    {
        cout << "GameSelectionHopping created" << endl;    // ctor
    }

    //! inline private non virtual dtor
    /** Inheritance is not allowed.
    This class kills itself in freeResources() */
    ~GameSelectionHopping()
    {
        cout << "GameSelectionHopping deleted" << endl;
    }

    //! inline private copy ctor
    /** prevent "accidental" copies */
    GameSelectionHopping( const GameSelectionHopping& );

    //! inline private assign operator
    /** prevent "accidental" assignments */
    GameSelectionHopping& operator=( const GameSelectionHopping& );

    //! init any general data, timers player/enemy/weapon states
    /** \return false if there any initialisation errors */
    bool initData();

    //! Init animated meshes and textures etc
    /** \return false if there any initialisation errors */
    bool initNodes();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUISkin();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUIComponents();

    //! init any gui data - after the gui objects are created
    /** \return false if there any initialisation errors */
    bool initGUIData();

    //! init the camera/s required for this level
    /** \return false if there any initialisation errors */
    bool initCameras();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initWorld();

    //! key events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void keyboardEvent( SEvent event );

    //! mouse events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void mouseEvent( SEvent event );

    //! a mouse event happened on a GUI object
    /** \param the event passed from Irrlicht's event handler */
    void mouseGUIEvent( SEvent event );

    //! show FPS, poly count, active camera position and time
    void debugData();

    //! the debug display
    void displayDebugData();
    //! if true show the debug display
    bool showDebugData;
    //! the debug display on a label
    gui::IGUIStaticText* pDebugData;

    //! update local time keeper
    void updateTimer();
    //! Stores the complete time that has passed for this level
    unsigned int gameTime;
    //! the actual time taken to render a scene
    unsigned int timeDelta;
    //! msecs since last frame - will need to adjust for pause / resume
    unsigned int timeLast;
    //! time now - will need to adjust for pause / resume
    unsigned int timeNow;
    //! desired render cycles per second
    unsigned int desiredFPS;

};

#endif // IRR_TEMPLATE_H


























