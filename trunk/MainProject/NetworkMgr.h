#ifndef NETWORKMGR_H
#define NETWORKMGR_H

#include <iostream>
#include <vector>


using std::cout;
using std::endl;

#include "irrlicht.h"
#include "IrrUtils.h"
#include "RakNetIncludes.h"

using namespace irr;

class NetworkMgr
{
public:
    /** Default inline constructor */
    NetworkMgr() {}
    /** Default inline destructor */
    virtual ~NetworkMgr() {}

    bool connect();
    bool update();
    bool transmitBullet( int weaponSelection, core::vector3df sStart, core::vector3df sEnd, RakNet::RakNetGUID guidmy );

    RakNet::BitStream bsShoot;

protected:
private:
};

#endif // NETWORKMGR_H
