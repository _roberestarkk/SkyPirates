#ifndef CONFIGMGR_H
#define CONFIGMGR_H

#include <iostream>
#include <string>
#include <map>

#include "Irrlicht.h"

#ifndef TIXML_USE_TICPP
#define TIXML_USE_TICPP
#endif
#include "ticpp.h"

#include "utils.h"
#include "xmlutils.h"

/** Checks if existing user preferences files exist and
  * populates STL maps with the user config data.
  * If the system files do not exist because it is first
  * time use or deleted, then uses hard coded settings. */
class DataMgr
{
public:
    DataMgr() {}
    virtual ~DataMgr() {}

    //! init the system settings for the game
    /**  \param name: the name of the required xml file
      * the default settings are hard coded and created in init() */
    void init( std::string fileName );

    //! get a current irrlicht system setting
    /** \param name: the name of the required index
      * \return the associated value stored with name */
    std::string getSystemSetting( std::string name ) const;

    //! set a current irrlicht system setting
    /** \param name: the name of the map index
      * \param value: the value to store at that index */
    void setSystemSetting( std::string name, std::string value );

    //! get a current irrlicht system setting
    /** \param name: of the index from DevOptions.xml, currently devices, resolutions and bpps
      * \return the associated value stored with name */
    std::vector<std::string> getDataList( std::string name );

    //! save the current irrlicht, sound and game settings
    /** \return true if all saved OK, false if failed- check log message */
    bool saveSystemSettings( );

private:
    // can not make a copy or assignment
    DataMgr( const DataMgr& other ) { }
    DataMgr& operator=( const DataMgr& rhs )
    {
        return *this;
    }

    //! read the current user settings or use the hard coded defaults
    /** the file need not exist, first run of game or deleted
      * \return true if all OK */
    bool readSystemData(  );

    //! read the fixed list data for the game config screen
    /** \param fileName: the XML file to open
      * \return true if all OK */
    bool readDataLists( std::string name );

    //! if there are no settings files then apply the hard coded alternatives
    void createDefaultConfigData( );

    //! the storage for the irr system settings
    std::map<std::string, std::string> systemSettings;
    //! the storage for the sound settings
    std::map<std::string, std::string> soundSettings;
    //! the storage for the game settings
    std::map<std::string, std::string> gameSettings;
    //! the file names for the xml data files
    std::string dataFileName;
    //! the list data for the config settings screen
    std::map< std::string, std::vector<std::string> > dataLists;

};

#endif // CONFIGMGR_H
