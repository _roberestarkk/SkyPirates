#ifndef ENTITY_H
#define ENTITY_H

// Class Created By: Sam Shannon.


#include <string>
#include <iostream>
using std::cout;
using std::endl;

#include "irrlicht.h"
using namespace irr;

#include "IrrUtils.h"


/** \brief A class used to handle Entity(things the player can 'pick up') based data. All Entities created are of this class-type.
 */
class Entity
{
public:
    /** Default constructor */
    Entity() {}
    /** Default destructor */
    virtual ~Entity() {}


    /** \brief A subroutine which allows the setting of the Entity's Position.
     * \param newPos is the new Position of the Entity
     */
    void setPosition( core::vector3df newPos );
    /** \brief A subroutine which allows the setting of the Entity's Rotation.
     * \param newRot is the new Rotation of the Entity.
     */
    void setRotation( core::vector3df newRot );
    /** \brief A subroutine which allows the setting of the Entity's Type.
     * \param newType is the new Type of the Entity.
     */
    void setType( char newType );
    /** \brief A subroutine which allows the setting of the Entity's Subtype.
     * \param newSubtype is the new Subtype of the Entity.
     */
    void setSubtype( char newSubtype );
    /** \brief A subroutine which allows the setting of the Entity's Value.
     * \param newVal is the new Value of the Entity.
     */
    void setValue( int newVal );
    /** \brief A subroutine which allows the setting of the Entity's Mesh.
     * \param newMesh is the pointer to the Entity's Mesh.
     */
    void setEntityMesh( scene::IMeshSceneNode* newMesh );
    /** \brief A subroutine which allows the setting of the Entity's respawn time.
     * \param newTimer is the value to test against the game time.
     */
    void setEntityTimer(unsigned int newTimer);


    /** \brief A function which will return the Position of the Entity.
     * \return The Entity's Position as a core::vector3df
     */
    core::vector3df getPosition();
    /** \brief A function which will return the Rotation of the Entity.
     * \return The Entity's Rotation as a core::vector3df
     */
    core::vector3df getRotation();
    /** \brief A function which will return the Type of the Entity.
     * \return The Entity's Type as a char[1]
     */
    char getType();
    /** \brief A function which will return the Subtype of the Entity.
     * \return The Entity's Subtype as a char[1]
     */
    char getSubtype();
    /** \brief A function which will return the Value of the Entity.
     * \return The Entity's Value as an int
     */
    int getValue();
    /** \brief A function which will return a pointer to the Mesh associated with the Entity.
     * \return The Mesh associated with the Entity as a scene::IMeshSceneNode*
     */
    scene::IMeshSceneNode* getEntityMesh();
    /** \brief A function which will return the time of reactivation for the Entity.
     * \return The point in time at which the Entity should reactivate.
     */
    unsigned int getEntityTimer();

protected:
    //! A variable which stores the Entity's Position.
    core::vector3df position;
    //! A variable which stores the Entity's Rotation.
    core::vector3df rotation;
    //! A variable which stores the Entity's Type (This will determine which aspect of the Player's Stats the Entity will affect).
    //! Possible Types are: 'a' for Ammo, 'p' for Powerup, 'f' for Flag and 'b' for Body.
    char type;
    //! A variable which stores the Entity's Subtype (This will determine a more specific aspect of the currently specified [Type] to affect).
    //! Possible Subtypes are: The first letter of each weapon for that type of ammo ('r' for Rifle), The first letter of each powerup for that type of Power, the letter 'h' for health and 'a' for armour.
    char subtype;
    //! A variable which stores the Entity's Value (This will determine the actual affect the Entity will have on the specified [Subtype] of the specified [Type] of the Player's Stats)
    int value;
    //! A variable which stores the pointer to the loaded mesh that is representing the Entity.
    scene::IMeshSceneNode* entityMesh;
    //! A variable which stores the point in time at which the Entity becomes active again.
    unsigned int respawnTime;

private:
};

#endif // ENTITY_H
