#ifndef GAMELOG_H
#define GAMELOG_H
#include <iostream>
#include <cstdarg>
#include <ctime>

static const int MAJOR_VERSION = 1;
static const int MINOR_VERSION = 0;
static const int BUILD_NUMBER = 2;

class GameLog
{
public:
    /** @brief GameLog directs logging to console or a given file
      * gameLog as default NULL will output to console.
    */
    GameLog( ) : gameLog( NULL ) {}

    //! inline dtor
    virtual ~GameLog() {}

    /** @brief initLog must be called if using file output.
    Otherwise it is optional, does provide total run time when used with closeLog()
      * @param fileName for log output or NULL will output to console
    */
    void initLog( const char * fileName );

    /** @brief print
      * adds a line to the log - uses printf() formatting.
      * Does not handle C++ string, wstring or wchar_t data types as arguments.
      * Use string.c_str() or the wide to one byte string functions in Utils.
      * @param format string containing output with text and %d %s etc
      * @param ... 0 to many variables for the place holders %d %s etc
      * @return the number of characters output or -1 if error occurred.
    * \note could just use C printf() directly if console only output.
    */
    int print( char * format, ... );

    /** @brief closeLog should be called if using file output.
      * Adds a time stamp to the log and closes log if writing to file
    */
    void closeLog( );

private:
    FILE *gameLog;
};


#endif // GAMELOG_H
