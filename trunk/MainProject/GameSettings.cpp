#include "GameSettings.h"

using namespace irr;

//! define the static singleton pointer
GameSettings GameSettings::instance;

void GameSettings::update()
{
    this->updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    if ( imgBG )
        pVideo->draw2DImage(
            imgBG,
            core::rect< irr::s32 >( 0, 0, pGameMgr->getScreenResolution().Width, pGameMgr->getScreenResolution().Height ),
            core::rect< irr::s32 >( 0, 0, imgBG->getSize().Width, imgBG->getSize().Height ) );
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
    pVideo->endScene();
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
}

bool GameSettings::initState( GameMgr& game )
{
    cout << "GameSettings initState() start" << endl;
    // init the local game engine reference
    pGameMgr = &game;
    pDataMgr = pGameMgr->getDataMgr();
    pDevice = pGameMgr->getDevice();
    // local references to the other Irrlicht system pointers
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    // set or disable mouse cursor
    pDevice->getCursorControl()->setVisible( true );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls may be important
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initGUISkin() )
        return false;
    if ( not this->initGUIComponents() )
        return false;
    if ( not this->initGUIData() )
        return false;
    if ( not this->initWorld()    )
        return false;
    if ( not this->initNodes()    )
        return false;
    if ( not this->initCameras()  )
        return false;
    fader->fadeIn( 3000 );
    cout << "GameSettings initState() success" << endl;
    return true;
}

bool GameSettings::initData()
{
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = false;
    cout << "GameSettings initData() success" << endl;
    return true;
}
wchar_t gameName[256] = L"";

bool GameSettings::initGUISkin()
{
    gui::IGUIFont* pFontGUI;
    //! load background image - it is drawn in the update function
    imgBG = pVideo->getTexture( "media/images/options.png" );
    //! set fader
    fader = pDevice->getGUIEnvironment()->addInOutFader();
    // the colour to start from
    fader->setColor( video::SColor( 0, 0, 0, 0 ) );
    // msecs for the fade to complete
    fader->fadeIn( 3000 );
    // skin renderers - EGST_WINDOWS_CLASSIC, EGST_WINDOWS_METALLIC or EGST_BURNING_SKIN
    gui::IGUISkin* pSkin = pGUIEnv->createSkin( gui::EGST_WINDOWS_METALLIC );
    //! changing some of the skin's colour scheme
    // font colour for buttons, checkbox, unselected items in lists etc
    pSkin->setColor( gui::EGDC_BUTTON_TEXT, video::SColor( 255, 255, 255, 255 ) );
    // background for selected items
    pSkin->setColor( gui::EGDC_HIGH_LIGHT , video::SColor( 255, 64, 64, 64 ) );
    // colour of selected items
    pSkin->setColor( gui::EGDC_HIGH_LIGHT_TEXT , video::SColor( 255, 0, 255, 255 ) );
    // face colour of buttons
    pSkin->setColor( gui::EGDC_3D_FACE, video::SColor( 255, 0, 60, 120 ) );
    // load fonts for gui, keep references if required later for changing fonts
    //! font for text labels, combo and lists etc
    pFontGUI = pGUIEnv->getFont( "media/fonts/LucidaUnicode16.png" );
    // the default font - used unless the object overrides it
    pSkin->setFont( pFontGUI, gui::EGDF_DEFAULT );
    // not changing tooltip font so not keeping a reference to it
    pSkin->setFont( pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" ), gui::EGDF_TOOLTIP );
    // different font for the buttons
    pSkin->setFont( pGUIEnv->getFont( "media/fonts/SansFont24.png" ), gui::EGDF_BUTTON );
    // the GUI environment will manage the pointer
    pGUIEnv->setSkin( pSkin );
    // must drop() as used a create...() function, createSkin().
    pSkin->drop();
    cout << "GameSettings initGUISkin() success" << endl;
    return true;
}

bool GameSettings::initGUIComponents()
{
    //! set up some common variables for the gui components
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // common distance from L/R edges of screen
    signed margin = scrnWidth / 16;
    // starting values for the first component, the title
    // object start top, width and height
    signed topY = 12, width = 320, height = 48;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );
    //! set title text and position
    gui::IGUIStaticText* temp;
    // text, size, border, wordwrap, parent, id, background
    temp = pGUIEnv->addStaticText ( L"The Big Title", sizer, false, false, 0, -1, false );
    // diferent colour and font for the title
    temp->setOverrideColor( video::SColor( 0, 0, 0, 0 ) );
    temp->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );
    temp->setTextAlignment( gui::EGUIA_CENTER, gui::EGUIA_CENTER );
    //! devices label and combo box
    leftX = margin;
    topY = 60;
    width = 192;
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    temp = pGUIEnv->addStaticText ( L"Video Driver", sizer, false, true, 0, -1, false ) ;
    temp->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    //! create device type list box
    topY = 96;            // move down
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    cboDev = pGUIEnv->addComboBox ( sizer, 0, GUI_CBO_DEV );
    //! create fullscreen check box
    topY += 128;    // move down
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    chkFullscreen = pGUIEnv->addCheckBox( false, sizer, 0,  GUI_CHK_FULLSCRN,
                                          L"Full Screen" );
    chkFullscreen->setToolTipText( L"Full Screen Mode" );
    //! bpp chk boxes
    topY += 64;
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    temp = pGUIEnv->addStaticText ( L"BPP", sizer, false, true, 0, -1, false ) ;
    temp->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    topY += 32;
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    chk16bpp = pGUIEnv->addCheckBox( false, sizer, 0, GUI_CHK_16, L"16 bpp" );
    leftX += 128;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    chk32bpp = pGUIEnv->addCheckBox( true, sizer, 0, GUI_CHK_32, L"32 bpp"  );
    //!  move over to the right and create resolutions list box
    leftX = scrnWidth - 128 - margin;
    topY = 60;
    width = 192;
    height = 32;
    leftX = scrnWidth - width - margin;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    temp = pGUIEnv->addStaticText ( L"Resolution", sizer, false, true, 0, -1, false ) ;
    temp->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    // move down
    topY = 96;
    height = 128;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    lstRes = pGUIEnv->addListBox( sizer, 0, GUI_LST_RES, true );
    //! create play sounds check box
    topY += 160;                 // move down
    height = 32;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    chkSoundOn = pGUIEnv->addCheckBox( false, sizer, 0,  GUI_CHK_SOUND_ON, L"Sound On" );
    chkSoundOn->setToolTipText( L"Turn sound effects on or off" );
    //! sound volume scroll bar
    topY += 64;     // move down
    leftX = scrnWidth / 2 + margin;
    width = scrnWidth - leftX - margin;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    scrVolume = pGUIEnv->addScrollBar( true, sizer, 0, GUI_SCR_VOLUME );
    scrVolume->setVisible(false);
    //! Game name text box
    topY += 120;     // move down
    leftX = scrnWidth / 2 + margin;
    width = scrnWidth - leftX - margin;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    txtName = pGUIEnv->addEditBox( gameName, sizer );
    //! create OK button
    leftX = 160;
    topY = scrnHeight - 60;
    width = 64;
    height = 48;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnOK = pGUIEnv->addButton( sizer, 0, GUI_BTN_OK, L"OK" );
    btnOK->setIsPushButton();
    //! create Cancel button
    width = 128;
    leftX = scrnWidth - 128 - width;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnCancel = pGUIEnv->addButton( sizer, 0, GUI_BTN_CANCEL, L"Cancel" );
    btnCancel->setIsPushButton();
    //! debug data display
    lblDebugData = pGUIEnv->addStaticText(
                       L"",
                       core::rect<s32>( 0, scrnHeight - 64, 256, scrnHeight ),
                       true,
                       true,
                       0,
                       -1,
                       true );
    lblDebugData->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    // starting off invisible for this - no poly count
    lblDebugData->setVisible( false );
    cout << "GameSettings initGUIComponents() success" << endl;
    return true;
}

bool GameSettings::initGUIData()
{
    std::vector<std::string> dataList;
    dataList = pDataMgr->getDataList( "devices" );
    unsigned count = 0;
    std::vector<std::string>::iterator iter;
    for ( iter = dataList.begin(); iter != dataList.end(); iter++, ++count )
    {
        // convert multibyte string to unicode wide char
        cboDev->addItem( Utils::wideString( *iter ).c_str() );
        if ( ( *iter ).find( pDataMgr->getSystemSetting( "device" ) ) != std::string::npos )
            cboDev->setSelected( count );
    }
    dataList = pDataMgr->getDataList( "resolutions" );
    count = 0;
    for ( iter = dataList.begin(); iter != dataList.end(); iter++, ++count )
    {
        // convert multibyte string to unicode wide char
        lstRes->addItem( Utils::wideString( *iter ).c_str() );
        if ( ( *iter ).find( pDataMgr->getSystemSetting( "width" ) ) != std::string::npos &&
                ( *iter ).find( pDataMgr->getSystemSetting( "height" ) ) != std::string::npos )
            lstRes->setSelected( count );
    }
    //! set the unrequired bpp check box from the game engine settings
    if ( pDataMgr->getSystemSetting( "bpp" ) == "16" )
    {
        chk16bpp->setChecked( true );
        chk32bpp->setChecked( false );
    }
    else
    {
        chk32bpp->setChecked( true );
        chk16bpp->setChecked( false );
    }
    const char* temp = pDataMgr->getSystemSetting( "name" ).c_str();
    wchar_t wcstring[256];
    mbstowcs( wcstring, temp, strlen( temp ) + 1 );
    txtName->setText( wcstring );
    cout << temp << endl;
    cout << pDataMgr->getSystemSetting( "name" ).c_str() << endl;
    cout << wcstring << endl;

    //! set check boxes from the game engine settings
    chkFullscreen->setChecked( pGameMgr->isFullScreen() );
    chkSoundOn->setChecked( Utils::toBoolean( pDataMgr->getSystemSetting( "soundon" ) ) );
    std::cout << "GameSettings initGUIData() success" << std::endl;
    return true;
}

bool GameSettings::initNodes()
{
//    // add the sun
//    sun = pSceneMgr->addSphereSceneNode( 120.0f, 360 );
//    sun->setMaterialFlag( video::EMF_LIGHTING, false );
//    sun->setMaterialTexture( 0, pVideo->getTexture( "media/textures/lavahell.png" ) ); // set diffuse texture
//    sun->setPosition( core::vector3df( 0, 0, 0 ) );
//    // spin the sun around it's axis
//    scene::ISceneNodeAnimator* anim = pSceneMgr->createRotationAnimator( core::vector3df( 0.0f, 2.0f, 0.0f ) );
//    sun->addAnimator( anim );
//    anim->drop();
//    // add earth
//    earth = pSceneMgr->addSphereSceneNode( 80.0f, 240 );
//    earth->setMaterialFlag( video::EMF_LIGHTING, false );
//    earth->setMaterialTexture( 0, pVideo->getTexture( "media/earth.bmp" ) );
//    // create the earth orbit
//    scene::ISceneNodeAnimator* anim1 = pSceneMgr->createFlyCircleAnimator(
//                                           sun->getPosition( ),
//                                           1000.0f,
//                                           0.001f,
//                                           core::vector3df( 0.40f, -1.0f, 0.0f )
//                                       );
//    earth->addAnimator( anim1 );
//    anim1->drop();
//    // spin the earth around it's axis
//    scene::ISceneNodeAnimator* anim2 = pSceneMgr->createRotationAnimator( core::vector3df( 0.0f, 4.0f, 0.0f ) );
//    earth->addAnimator( anim2 );
//    anim2->drop();
//    // add the moon
//    moon = pSceneMgr->addSphereSceneNode(
//               40.0f,      // radius
//               64,         // poly count
//               earth,       // parent
//               0.008f
//           );
//    moon->setMaterialFlag( video::EMF_LIGHTING, false );
//    moon->setMaterialTexture( 0, pVideo->getTexture( "media/rock.bmp" ) );
//    scene::ISceneNodeAnimator* anim3 = pSceneMgr->createFlyCircleAnimator(
//                                           earth->getPosition( ),
//                                           300.0f,
//                                           0.01f,
//                                           core::vector3df( 0.0f, -1.0f, 0.0f )
//                                       );
//    moon->addAnimator( anim3 );
//    anim3->drop();
    return true;
}

bool GameSettings::initCameras()
{
    pCamera = pSceneMgr->addCameraSceneNode(
              );
    pCamera->setPosition( core::vector3df( 0, 0, -1600 ) );
    pCamera->setTarget( core::vector3df( 0, 0, 0 ) );
    return true;
}

bool GameSettings::initWorld()
{
//    pVideo->setTextureCreationFlag( video::ETCF_CREATE_MIP_MAPS, false );
//    pSceneMgr->addSkyBoxSceneNode(
//        pVideo->getTexture( "media/sky.png" ),
//        pVideo->getTexture( "media/sky.png" ),
//        pVideo->getTexture( "media/sky.png" ),
//        pVideo->getTexture( "media/sky.png" ),
//        pVideo->getTexture( "media/sky.png" ),
//        pVideo->getTexture( "media/sky.png" )
//    );
    return true;
}

void GameSettings::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
        case irr::KEY_RETURN:
            if ( dataChanged )
            {
                // change the datamgr contents to the user selection/s
                cout << "SAVING NEW SETTINGS" << endl;
                saveNewSettings();
                cout << "SAVED NEW SETTINGS" << endl;
                // save to the xml file
                pDataMgr->saveSystemSettings();
                cout << "WRITTEN NEW SETTINGS" << endl;
                // clean up this level, reset the system to new settings
                this->freeResources();
                pGameMgr->resetGameMgr();
            }
            break;
        case irr::KEY_F8:
            pGameMgr->changeState( StartMenu::getInstance() );
            break;
        case irr::KEY_F9:
            pGameMgr->changeState( GameSelection::getInstance() );
            break;
        case irr::KEY_F10:
            pGameMgr->changeState( LevelSelection::getInstance() );
            break;
        case irr::KEY_F11:
            pGameMgr->changeState( LvlDerelict::getInstance() );
            break;
        case irr::KEY_F12:
            pGameMgr->changeState( LvlHopping::getInstance() );
            break;
        case irr::KEY_F7:
            pGameMgr->changeState( GameSettings::getInstance() );
            break;
            // and reload the opening state
            break;
        case irr::KEY_ESCAPE:
            // nothing changed so reload the opening state
            pGameMgr->changeState( StartMenu::getInstance() );
            break;
        case irr::KEY_F2: // switch wire frame mode
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, !terrain->getMaterial(0).Wireframe);
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, false);
            break;
        case irr::KEY_F3: // switch points only mode
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, !terrain->getMaterial(0).PointCloud);
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, false);
            break;
        case irr::KEY_F4: // toggle detail map
//            terrain->setMaterialType(
//                terrain->getMaterial(0).MaterialType == video::EMT_SOLID ?
//                video::EMT_DETAIL_MAP : video::EMT_SOLID);
            break;
        default :
            break;
        }
    }
}

void GameSettings::mouseEvent( SEvent event )
{
}

void GameSettings::mouseGUIEvent( SEvent event )
{
    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_LISTBOX_CHANGED :
    case gui::EGET_COMBO_BOX_CHANGED :
    case gui::EGET_CHECKBOX_CHANGED :
    case gui::EGET_EDITBOX_CHANGED :
        dataChanged = true;
        if ( id == GUI_CHK_16 )
        {
            chk16bpp->setChecked( true );
            chk32bpp->setChecked( false );
        }
        else if ( id == GUI_CHK_32 )
        {
            chk16bpp->setChecked( false );
            chk32bpp->setChecked( true );
        }
        break;
    case gui::EGET_BUTTON_CLICKED:
        if ( id == GUI_BTN_OK )
        {
            if ( dataChanged )
            {
                // change the datamgr contents to the user selection/s
                saveNewSettings();
                // save to the xml file
                pDataMgr->saveSystemSettings();
                // clean up this level, reset the system to new settings
                this->freeResources();
                pGameMgr->resetGameMgr();
            }
            // and reload the opening state
//
        }
        if ( id == GUI_BTN_CANCEL )
        {
            // nothing changed so reload the opening state
            pGameMgr->changeState( StartMenu::getInstance() );
        }
        break;
    default :
        break;
    }// end switch: type of GUI event
}

void GameSettings::freeResources( )
{
    // clear the gui environment unless there are elements to use in the next state
    pGUIEnv->clear();
    // same with the scene manager
    pSceneMgr->clear();
    cout << "GameSettings freeResources()" << endl;
}


void GameSettings::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting ability
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
//    if ( pCamera )
//    {
//        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
//                 pCamera->getPosition().X,
//                 pCamera->getPosition().Y,
//                 pCamera->getPosition().Z );
//    }
//    else
    sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    lblDebugData->setText( wstr.c_str() );
}

void GameSettings::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

bool GameSettings::saveNewSettings(  )
{
    cout << "Started Saving Stuff" << endl;
    pDataMgr->setSystemSetting( "device", Utils::stdString( std::wstring( cboDev->getText() ) ) );
    // hard coded 16 or 32
    if ( chk16bpp->isChecked () )
        pDataMgr->setSystemSetting( "bpp", "16" );
    else
        pDataMgr->setSystemSetting( "bpp", "32" );
    // get the selected item from the list - not error checking that anything is selected
    std::string temp = Utils::stdString( std::wstring( lstRes->getListItem ( lstRes->getSelected() ) ) );
    // width and height are joined with  " x " in the list so need to split it
    // this array will hold the two strings that are delimited (separated) by the " x "
    std::vector<std::string> vec;
    // splitString() will log an error if char not found
    Utils::splitString( temp, 'x', vec );
    // check vector has content and has 2 returned strings
    if ( ! vec.empty() && vec.size() == 2 )
    {
        // trim any extra spaces off the strings and save them
        pDataMgr->setSystemSetting( "width", Utils::trim( vec.at( 0 ) ) );
        pDataMgr->setSystemSetting( "height", Utils::trim( vec.at( 1 ) ) );
    }
    pDataMgr->setSystemSetting( "soundon", Utils::toString<bool>( chkSoundOn->isChecked () ) );
    pDataMgr->setSystemSetting( "fullscreen", Utils::toString<bool>( chkFullscreen->isChecked () )  );

    pDataMgr->setSystemSetting( "name", Utils::stdString( txtName->getText() ) );

    cout << "GameSettings saveNewSettings() success" << endl;
    return true;
}

void GameSettings::pause()
{
}

void GameSettings::resume()
{
}


























