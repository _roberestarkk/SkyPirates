#ifndef GAME_SETTINGS
#define GAME_SETTINGS

#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::wstring;

#include "irrlicht.h"
// everything in the Irrlicht Engine is in the 'irr' namespace
using namespace irr;
// the 5 sub namespaces of the 'irr' namespace
//using namespace core;
//using namespace scene;
//using namespace video;
//using namespace io;
//using namespace gui;

// string handling functions and string <-> number conversions
#include "Utils.h"
// stub class for game engine
#include "GameMgr.h"
#include "DataMgr.h"
// include any classes that this class navigates too.
#include "StartMenu.h"

/** this class uses the Singleton pattern to ensure there is only ever one instance.
  This class will (eventually) be created by a State Manager class.
  This class is for a single level within a game.
  There can be multiple levels but only one active at any time */
class GameSettings : public IState
{
private:

    enum GUIobjects
    {
        GUI_BTN_OK = 1001,              // save(if need to) & quit
        GUI_BTN_CANCEL,                 // no save and quit
        GUI_CBO_DEV,                    // list of devices
        GUI_LST_RES,                    // list of resolutions
        GUI_SCR_VOLUME,                 // scroll bar for volume setting
        GUI_CHK_16,                     // list of color depths
        GUI_CHK_32,                     // list of color depths
        GUI_CHK_FULLSCRN,               // check box - fullscreen
        GUI_CHK_SOUND_ON,               // check box - play sound
        GUI_TXT_GAME_NAME               // game name editbox
    };
    // only need a reference to gui components that require a response to events
    //! the gui object references
    gui::IGUIButton* btnOK;             // command buttons
    gui::IGUIButton* btnCancel;

    gui::IGUIComboBox* cboDev;          // list boxes
    gui::IGUIListBox* lstRes;



    gui::IGUICheckBox* chk16bpp;        // check boxes
    gui::IGUICheckBox* chk32bpp;

    gui::IGUICheckBox* chkFullscreen;   // check boxes
    gui::IGUICheckBox* chkSoundOn;

    gui::IGUIScrollBar* scrVolume;
    video::ITexture* imgBG;          // bg image

    gui::IGUIInOutFader* fader;         // start/finish fader after resource loading
    // save user settings if data changed
    bool saveNewSettings( );
    bool dataChanged;

    // nodes for the scene
    scene::ISceneNode* sun;
    scene::ISceneNode* earth;
    scene::ISceneNode* moon;

    scene::ICameraSceneNode* pCamera;

public:

    gui::IGUIEditBox* txtName;          // edit box


    //! Initialise the game objects. Calls the private init...() functions.
    /** The State Manager class will take over most of the device initialisation
    as there should only be a single reference to each of the Irrlicht devices
    \param the title for the window
    \return false if there are any initialisation errors */
    bool initState( GameMgr& game );

    //! free any resources on the heap
    /** delete any objects created by new and drop any Irrlicht objects
    made by functions beginning with create, such as createDevice() */
    void freeResources( );

    //! The main game loop update, call any other update functions from here.
    /** Anything can be drawn between a beginScene() and endScene() call.
    The beginScene clears the screen with a color and depth buffer if wanted.
    The Scene Manager and the GUI Environment then draw their content.
    The endScene() call presents everything on the screen. */
    void update();

    //! timer pause
    void pause();
    //! timer resume
    void resume();

    //! singleton class creation
    static GameSettings* getInstance()
    {
        return &instance;
    }

private:
    //! static instance reference
    static GameSettings instance;

    //! inline private ctor
    /** prevent "accidental" attempts to create an instance of the class */
    GameSettings( )
    {
        cout << "GameSettings created" << endl;    // ctor
    }

    //! inline private non virtual dtor
    /** Inheritance is not allowed.
    This class kills itself in freeResources() */
    ~GameSettings()
    {
        cout << "GameSettings deleted" << endl;
    }

    //! inline private copy ctor
    /** prevent "accidental" copies */
    GameSettings( const GameSettings& );

    //! inline private assign operator
    /** prevent "accidental" assignments */
    GameSettings& operator=( const GameSettings& );

    //! init any general data, timers player/enemy/weapon states
    /** \return false if there any initialisation errors */
    bool initData();

    //! Init animated meshes and textures etc
    /** \return false if there any initialisation errors */
    bool initNodes();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUISkin();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUIComponents();

    //! init any gui data - after the gui objects are created
    /** \return false if there any initialisation errors */
    bool initGUIData();

    //! init the camera/s required for this level
    /** \return false if there any initialisation errors */
    bool initCameras();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initWorld();

    //! key events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void keyboardEvent( SEvent event );

    //! mouse events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void mouseEvent( SEvent event );

    //! a mouse event happened on a GUI object
    /** \param the event passed from Irrlicht's event handler */
    void mouseGUIEvent( SEvent event );

    //! show FPS, poly count, active camera position and time
    void debugData();

    //! the debug display
    void displayDebugData();
    //! if true show the debug display
    bool showDebugData;
    //! the debug display on a label
    gui::IGUIStaticText* lblDebugData;

    //! update local time keeper
    void updateTimer();
    //! Stores the complete time that has passed for this level
    unsigned int gameTime;
    //! the actual time taken to render a scene
    unsigned int timeDelta;
    //! msecs since last frame - will need to adjust for pause / resume
    unsigned int timeLast;
    //! time now - will need to adjust for pause / resume
    unsigned int timeNow;
    //! desired render cycles per second
    unsigned int desiredFPS;

};

#endif // IRR_TEMPLATE_H


























