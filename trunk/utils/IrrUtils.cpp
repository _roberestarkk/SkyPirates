#include "IrrUtils.h"
#include "Utils.h"

void rotateNode( irr::scene::ISceneNode *node, irr::core::vector3df rot )
{
    core::matrix4 m;
    m.setRotationDegrees( node->getRotation() );
    core::matrix4 n;
    n.setRotationDegrees( rot );
    m *= n;
    node->setRotation( m.getRotationDegrees() );
    node->updateAbsolutePosition();
}


void translateNode( scene::ISceneNode *node, core::vector3df vel )
{
    core::matrix4 m;
    m.setRotationDegrees( node->getRotation() );
    m.transformVect( vel );
    node->setPosition( node->getPosition() + vel );
    node->updateAbsolutePosition();
}

void setFogForAllCurrentMeshes( scene::ISceneManager* pSceneMgr )
{
    scene::ISceneNode* node = 0;
    const core::list<scene::ISceneNode*>& list = pSceneMgr->getRootSceneNode()->getChildren();
    core::list<scene::ISceneNode*>::ConstIterator begin = list.begin();
    core::list<scene::ISceneNode*>::ConstIterator end   = pSceneMgr->getRootSceneNode()->getChildren().end();
    while ( begin != end )
    {

        if ( ( 0 != ( node = pSceneMgr->getSceneNodeFromType( scene::ESNT_MESH, *begin ) ) ) ||
                ( 0 != ( node = pSceneMgr->getSceneNodeFromType( scene::ESNT_CUBE, *begin ) ) ) ||
                ( 0 != ( node = pSceneMgr->getSceneNodeFromType( scene::ESNT_TERRAIN, *begin ) ) ) )
        {
            scene::IMeshSceneNode* msn = ( scene::IMeshSceneNode* ) node;
            msn->setMaterialFlag( video::EMF_FOG_ENABLE, true );
        }

        begin++;
    }
}


void setLightingForAllCurrentMeshes( scene::ISceneManager* pSceneMgr )
{
    scene::ISceneNode* node = 0;
    const core::list<scene::ISceneNode*>& list = pSceneMgr->getRootSceneNode()->getChildren();
    core::list<scene::ISceneNode*>::ConstIterator begin = list.begin();
    core::list<scene::ISceneNode*>::ConstIterator end   = pSceneMgr->getRootSceneNode()->getChildren().end();
    while ( begin != end )
    {

        //(*begin)->setMaterialType(video::EMT_);
        ( *begin )->setMaterialFlag( video::EMF_NORMALIZE_NORMALS, true );
        //(*begin)->setMaterialFlag(video::EMF_LIGHTING, true);
        //(*begin)->setMaterialType(video::EMT_DETAIL_MAP);

        //(*begin)->setMaterialType(video::EMT_TRANSPARENT_VERTEX_ALPHA);

        if ( ( 0 != ( node = pSceneMgr->getSceneNodeFromType( scene::ESNT_TERRAIN, *begin ) ) ) ||
                ( 0 != ( node = pSceneMgr->getSceneNodeFromType( scene::ESNT_CUBE, *begin ) ) ) ) //||
            //	( 0 != (node = pSceneMgr->getSceneNodeFromType( scene::ESNT_MESH, *begin ) ) ) )
        {
            scene::IMeshSceneNode* msn = ( scene::IMeshSceneNode* ) node;
            msn->setMaterialFlag( video::EMF_LIGHTING, true );
            msn->setMaterialFlag( video::EMF_NORMALIZE_NORMALS, true );
        }

        begin++;
    }
}

// alternative recursive search
scene::ISceneNodeAnimatorCollisionResponse* addAllCurrentMeshesForCollision( scene::ISceneManager* pSceneMgr, scene::ICameraSceneNode* camera )
{
    managerAssignTriangleSelectors( pSceneMgr );
    // recursively gather triangle selectors into a meta selector
    scene::IMetaTriangleSelector* meta = gatherAllTriangles( pSceneMgr );
    // create collision response using all objects for collision detection
    scene::ISceneNodeAnimatorCollisionResponse* anim =
        pSceneMgr->createCollisionResponseAnimator(
            meta,                                   // world to apply collision
            camera
        );

    camera->addAnimator( anim );
    // drop the meta selector. it is now owned by the animator
    meta->drop();
    return anim;
}

scene::IMetaTriangleSelector* gatherAllTriangles( scene::ISceneManager* pSceneMgr )
{
    managerAssignTriangleSelectors( pSceneMgr );
    // recursively gather triangle selectors into a meta selector
    scene::IMetaTriangleSelector* meta = pSceneMgr->createMetaTriangleSelector();
    managerGatherTriangleSelectors( pSceneMgr, meta );
    // return the meta selector - it MUST BE DROPPED
    return meta;
}

void managerAssignTriangleSelectors( scene::ISceneManager* pSceneMgr )
{
    nodeAssignTriangleSelectors( pSceneMgr->getRootSceneNode(), pSceneMgr );
}

void nodeAssignTriangleSelectors( scene::ISceneNode* node, scene::ISceneManager* pSceneMgr )
{
    // assign a selector for this node
    nodeAssignTriangleSelector( node, pSceneMgr );

    // now recurse on children...
    core::list<scene::ISceneNode*>::ConstIterator begin = node->getChildren().begin();
    core::list<scene::ISceneNode*>::ConstIterator end   = node->getChildren().end();

    for ( /**/; begin != end; ++begin )
        nodeAssignTriangleSelectors( *begin, pSceneMgr );
}


void managerGatherTriangleSelectors( scene::ISceneManager* pSceneMgr, scene::IMetaTriangleSelector* meta )
{
    nodeGatherTriangleSelectors( pSceneMgr->getRootSceneNode(), meta );
}

void nodeGatherTriangleSelectors( scene::ISceneNode* node, scene::IMetaTriangleSelector* meta )
{
    scene::ITriangleSelector* selector = node->getTriangleSelector();
    if ( selector )
        meta->addTriangleSelector( selector );

    // now recurse on children...
    core::list<scene::ISceneNode*>::ConstIterator begin = node->getChildren().begin();
    core::list<scene::ISceneNode*>::ConstIterator end   = node->getChildren().end();

    for ( /**/; begin != end; ++begin )
        nodeGatherTriangleSelectors( *begin, meta );
}

void nodeAssignTriangleSelector( scene::ISceneNode* node, scene::ISceneManager* pSceneMgr )
{
    if ( not node or not pSceneMgr )
        return;
    if ( node->getType() == scene::ESNT_MESH )
    {
        scene::IMeshSceneNode* msn = ( scene::IMeshSceneNode* )node;
        printf( " %s  ID:%i\n", msn->getName(), msn->getID() );
        if ( msn->getMesh() )
        {
            scene::ITriangleSelector* selector =
                pSceneMgr->createTriangleSelector( msn->getMesh(), msn );
            msn->setTriangleSelector( selector );
            selector->drop();
        }
    }
    else if ( node->getType() == scene::ESNT_ANIMATED_MESH )
    {
        scene::IAnimatedMeshSceneNode* msn = ( scene::IAnimatedMeshSceneNode* )node;
        printf( " %s  ID:%i\n", msn->getName(), msn->getID() );
        scene::IAnimatedMesh* am = msn->getMesh();
        if ( am )
        {
            scene::ITriangleSelector* selector =
                pSceneMgr->createTriangleSelector( am->getMesh( 0 ), msn );
            msn->setTriangleSelector( selector );
            selector->drop();
        }
    }
    else if ( node->getType() == scene::ESNT_TERRAIN )
    {
        scene::ITerrainSceneNode* tsn = ( scene::ITerrainSceneNode* )node;
        printf( " %s  ID:%i\n", tsn->getName(), tsn->getID() );
        scene::ITriangleSelector* selector =
            pSceneMgr->createTerrainTriangleSelector( tsn, 0 ); // probably don't want lod 0 all the time...
        tsn->setTriangleSelector( selector );
        selector->drop();
    }
    else if ( node->getType() == scene::ESNT_CUBE )
    {
        printf( " CUBE\n" );
        scene::ITriangleSelector* selector =
            pSceneMgr->createTriangleSelectorFromBoundingBox( node );
        node->setTriangleSelector( selector );
        selector->drop();
    }
    else
    {
        // not something we want to collide with
    }
}


