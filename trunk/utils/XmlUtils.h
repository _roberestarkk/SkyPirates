#ifndef TICPP_UTILS_H
#define TICPP_UTILS_H

#include <iostream>
#include <string>

#ifndef TIXML_USE_TICPP
#define TIXML_USE_TICPP
#endif
#include "ticpp.h"

class XmlUtils
{
public:
    static std::string getElementName( ticpp::Element node )
    {
        std::ostringstream oss;
        // extract the element content via the << overloaded operator
        oss << node;
        // convert to string using the oss function
        std::string name = oss.str();
        // find the first > character, starting from the first char in the string
        std::string::size_type location = name.find( ">", 0 );
        // if can't find a > then not a valid element name
        if( location != std::string::npos )
            // ignore the first < at 0 and get all text up to, but not including, the >'s
            name = name.substr( 1, location - 1 );
        else
            std::cout << "Didn't find a valid element name!" << std::endl;
        return name;
    }
};

#endif // TICPPUTILS_H
